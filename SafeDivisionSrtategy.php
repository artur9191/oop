<?php
/**
 * Created by PhpStorm.
 * User: arthur
 * Date: 20.10.16
 * Time: 15:49
 */

namespace calc;


interface SafeDivisionStrategy
{
    public function division($a, $b);
}

class SafeDivisionStrategyOn implements SafeDivisionStrategy
{
    public function division($a, $b) {
        return CalcSafe::division($a, $b);
    }

}

class SafeDivisionStrategyOff implements SafeDivisionStrategy
{
    public function division($a, $b) {
        return Calc4Operation::division($a, $b);
    }
}