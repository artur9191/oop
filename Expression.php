<?php
/**
 * Created by PhpStorm.
 * User: arthur
 * Date: 20.10.16
 * Time: 14:18
 */

namespace calc;

use calc\CalcSafe;
use calc\safeDivisionStrategy;

class Expression
{

    private $start;

    private $safeDivisionStrategy;

    function __construct($startValue, safeDivisionStrategy $strategy)
    {
        $this->start = $startValue;
        $this->safeDivisionStrategy = $strategy;
    }

    public function sum($b) {
        $this->start =  CalcSafe::addition($this->start, $b);

        return $this;
    }

    public  function  subtraction($b) {
        $this->start = CalcSafe::subtraction($this->start, $b);

        return $this;

    }

    public  function  multiplication($b) {
        $this->start = CalcSafe::multiplication($this->start, $b);

        return $this;

    }
    public  function  division($b) {
        $this->start = $this->safeDivisionStrategy->division($this->start, $b);

        return $this;

    }

    public function getResult() {
        return $this->start;
    }

}