<?php
/**
 * Created by PhpStorm.
 * User: arthur
 * Date: 21.10.16
 * Time: 1:31
 */

namespace calc;

abstract class Command {
    public abstract static function Execute($a, $b);
    public abstract static function Cancel($result, $operator);
}


class Sum extends Command
{
    public static function Execute($a, $b)
    {
        return $a + $b;
    }

    public static function Cancel($result, $operator)
    {
        return $result - $operator;
    }
}

class Subtraction extends Command
{
    public static function Execute($a, $b)
    {
        return $a - $b;
    }

    public static function Cancel($result, $operator)
    {
        return $result + $operator;
    }
}

class Multiplication extends Command
{
    public static function Execute($a, $b)
    {
        return $a * $b;
    }

    public static function Cancel($result, $operator)
    {
        return $result / $operator;
    }
}


class CalcCommand
{
    public $back = [];
    public $forward = [];

    private $current;

    public function Sum($a, $b) {
        Sum::Execute($a, $b);
        $this->back[count($this->back)] = ['sum', $b];
    }

    public function ToBack()
    {
        $command = $this->back[count($this->back) - 1];
        array_pop($this->back);
        array_push($this->forward, [$command]);

        $command[0]::Cansel($this->current, $command[1]);

    }



}




